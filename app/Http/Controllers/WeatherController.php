<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Controllers*/
Use App\Http\Controllers\YahooController;

/*Models*/
use App\Models\Weather;

class WeatherController extends Controller
{
	public function index()
	{ 
		$YahooController = new YahooController; 
		$CurrentObervations = (Object) [
			'miami' => $YahooController->cUrl_Connect('miami')->current_observation,
			'orlando' => $YahooController->cUrl_Connect('orlando')->current_observation,
			'new_york' => $YahooController->cUrl_Connect('new york')->current_observation,
		]; 

		return view('home.index',compact('CurrentObervations'));
	}

	public function show($city)
	{ 
		$city = str_replace('-', ' ', $city);
		$YahooController = new YahooController; 
		$Weathers = Weather::all()->where('city', $city); 
		$WeathersArray = array_values($Weathers->toArray()); 
		return view('home.weather_history',compact('Weathers', 'WeathersArray', 'city'));
	}
}
