<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*Models*/
use App\Models\Weather;

class YahooController extends Controller
{
	public function buildBaseString($baseURI, $method, $params) 
	{
		$r = array();
		ksort($params);
		foreach($params as $key => $value) {
			$r[] = "$key=" . rawurlencode($value);
		}
		return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
	}

	public function buildAuthorizationHeader($oauth) 
	{
		$r = 'Authorization: OAuth ';
		$values = array();
		foreach($oauth as $key=>$value) {
			$values[] = "$key=\"" . rawurlencode($value) . "\"";
		}
		$r .= implode(', ', $values);
		return $r;
	} 

  public function cUrl_Connect($city='new york')
  {  
  	$url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
  	$app_id = 'hqzrWNx4';
  	$consumer_key = 'dj0yJmk9dU5iS0REbG94azlNJmQ9WVdrOWFIRjZjbGRPZURRbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTNk';
  	$consumer_secret = '9490ebbadae8440a5e550a692f8000fee5734074';

  	$query = array(
  		'location' => $city,
  		'format' => 'json',
  	);

  	$oauth = array(
  		'oauth_consumer_key' => $consumer_key,
  		'oauth_nonce' => uniqid(mt_rand(1, 1000)),
  		'oauth_signature_method' => 'HMAC-SHA1',
  		'oauth_timestamp' => time(),
  		'oauth_version' => '1.0'
  	);

  	$base_info = $this->buildBaseString($url, 'GET', array_merge($query, $oauth));
  	$composite_key = rawurlencode($consumer_secret) . '&';
  	$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
  	$oauth['oauth_signature'] = $oauth_signature;

  	$header = array(
  		$this->buildAuthorizationHeader($oauth),
  		'X-Yahoo-App-Id: ' . $app_id
  	);
  	$options = array(
  		CURLOPT_HTTPHEADER => $header,
  		CURLOPT_HEADER => false,
  		CURLOPT_URL => $url . '?' . http_build_query($query),
  		CURLOPT_RETURNTRANSFER => true,
  		CURLOPT_SSL_VERIFYPEER => false
  	);

  	$ch = curl_init();
  	curl_setopt_array($ch, $options);
  	$response = curl_exec($ch);
  	curl_close($ch); 
  	$return_data = json_decode($response); 
  	$return_data->current_observation->pubDate = date('Y-m-d', $return_data->current_observation->pubDate);

  	$Weather = Weather::where('city', $city)->whereDate('date', $return_data->current_observation->pubDate)->get();
  	if (!$Weather->count()) {
  		Weather::create([
  			'city' => $city,
  			'date' => $return_data->current_observation->pubDate,
  			'observations' => $return_data->current_observation
  		]);
  	}

  	return $return_data;
  } 
}
