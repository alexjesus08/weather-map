<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{ 
	protected $table = 'weathers'; 
	protected $casts = [ 'observations' => 'collection' ];
  protected $fillable = [ 
    'city',
    'date', 
    'observations' 
  ];   
}