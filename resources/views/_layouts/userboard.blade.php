<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> 
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Global Styles -->
    <link rel="stylesheet" href="/css/style.css">
    @yield('page_css') 
    <title>Weather Map</title>
  </head>
  <body> 

  	<div class="container mt-2">
	   	<nav class="navbar navbar-expand-lg navbar-light">
	  		<div class="container-fluid">
	  			<a href="/">
	  				<img data-src="https://viajemosappprodstorage-cdn.azureedge.net/viajemosmx/skin/frontend/viajemos/default/images/viajemos-logo.png" alt="Viajemos.com - online Travel Agency" src="https://viajemosappprodstorage-cdn.azureedge.net/viajemosmx/skin/frontend/viajemos/default/images/viajemos-logo.png" class="navbar-brand" height="40">
	  			</a>
	  			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	  				<span class="navbar-toggler-icon"></span>
	  			</button>
	  			<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
	  				<ul class="navbar-nav mx-lg-3 mb-2 mb-lg-0">
	  					<li class="nav-item">
	  						<a class="nav-link active" aria-current="page" href="/">Inicio</a>
	  					</li> 
	  					<li class="nav-item dropdown">
	  						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
	  							Historial climatico
	  						</a>
	  						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
	  							<li><a class="dropdown-item" href="/show/miami">Miami</a></li>
	  							<li><a class="dropdown-item" href="/show/orlando">Orlando</a></li>
	  							<li><a class="dropdown-item" href="/show/new-york">New York</a></li> 
	  						</ul>
	  					</li> 
	  				</ul>
	  				<!-- <form class="d-flex">
	  					<input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
	  					<button class="btn btn-outline-success" type="submit">Buscar</button>
	  				</form> -->
	  			</div>
	  		</div>
	  	</nav>
	  	<div id="wrap-content">
	  		@yield('content')
	  	</div> 
    </div>

    <!-- Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>  
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> 
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="/js/helpers.js"></script>

    @yield('page_js') 
    @stack('modules_script')
  </body>
</html>
