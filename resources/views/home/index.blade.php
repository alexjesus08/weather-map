@extends('_layouts.userboard')
@section('page_css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
crossorigin=""/>
@endsection

@section('content')	
<div class="">
	<h4>¡Que no te vuelva tomar desprevenido el mal tiempo!</h4>
	<p>Ahora te damos la posibilidad de saber el clima actual en el destino al que viajas, y así puedes ir preparado para disfrutar de la ciudad o llegar rápido a tu cita de trabajo.</p>

	<div class=" mb-3">		
		<label for="inputPassword" class="col-sm-2 col-form-label">Selecciona la ciudad</label>
		<select class="city" name="city">
			<option value="miami" selected>Miami</option> 
			<option value="orlando">Orlando</option>
			<option value="new_york">New York</option>
		</select>
	</div>
</div>

<div class="mb-3">
	<div id="map" class="rounded"></div>	
</div>
@endsection

@section('page_js')
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
crossorigin=""></script>
<script>
	$(document).ready(function($) {
		/*Defino variables*/
		var markers = [];
		var current_obervations = @json($CurrentObervations); 
		var cities = {
			'miami': [25.782551,-80.221748],
			'orlando': [28.479321,-81.344292],
			'new_york': [40.71455,-74.007118]
		};

		/*Inicializo librerias*/
		$('.city').select2(); 
		var map = L.map('map').setView([51.505, -0.09], 13); 
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(map);

		Object.entries(cities).forEach(entry => {   
			const [city_name, latLng] = entry; 
			var observations = current_obervations[city_name]; 
			var marker = L.marker(latLng).addTo(map).bindPopup(get_collapse(observations,city_name));
			markers.push({'city':city_name, 'marker':marker});
		}); 
		map.flyTo(cities.miami, 10);

		/*Eventos front*/
		$('.city').on('select2:select', function (e) {
			var data = e.params.data;
			var city_name = $(data.element).val(); 
			map.flyTo(cities[city_name], 10);
			var marker = markers.filter(({city}) => { return city == city_name})[0];
			marker.marker.openPopup();
		}); 
	});
</script>
@endsection


