@extends('_layouts.userboard')
@section('page_css') 
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" /> 
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css" /> 
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" /> 
@endsection

@section('content')	
<div class="mb-4">
	<h4>Historico del clima en la ciudad de <span class="text-capitalize">{{$city}}</span></h4> 
</div>

<div class="mb-3">
	<table id="table_id" class="display">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Viento</th>
				<th>Atmósfera</th>
				<th>Astronomía</th>
				<th>Condición</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody> 
			@forelse($Weathers as $Weather)
				@php $Observations = $Weather->observations; @endphp
				<tr>
					<td>{{$Observations['pubDate']}}</td>
					<td><strong>chill</strong> {{$Observations['wind']['chill']}} ...</td>
					<td><strong>humidity</strong> {{$Observations['atmosphere']['humidity']}} ...</td>
					<td><strong>sunrise</strong> {{$Observations['astronomy']['sunrise']}} ...</td>
					<td><strong>temperature</strong> {{$Observations['condition']['temperature']}} ...</td>
					<td><button class="btn btn-primary see-more" data-date="{{$Observations['pubDate']}}">Ver Detalle</button></td>
				</tr> 
			@empty
				<tr>
					<td>No tenemos registros aún</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr> 
			@endforelse 
		</tbody>
	</table>
</div>
 

<div id="exampleModalCenteredScrollable" class="modal">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenteredScrollableTitle">Detalle del clima <small class="text-muted"></small></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body"> 

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button> 
      </div>
    </div>
  </div>
</div>

@endsection

@section('page_js')
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script> 
<script>
	$(document).ready(function($) {
		/*Defino variables*/ 
		var weathers = @json($WeathersArray);  
		var this_city = @json($city); 
		$('#table_id').DataTable({
			responsive: true,
			language: {"url": "//cdn.datatables.net/plug-ins/1.10.22/i18n/Spanish.json"}
		});

		/*Eventos frontend*/
		$(document).on('click', '.see-more', function (e) { 
			var this_date = this.dataset.date;
			var observations = weathers.filter(({city,date}) => { return (city == this_city && date == this_date)})[0].observations;
			$('#exampleModalCenteredScrollable .modal-title .text-muted').html(' ('+this_date+')'); 
			$('#exampleModalCenteredScrollable .modal-body').html(get_collapse(observations,this_city)); 
			$('#exampleModalCenteredScrollable').show();
		});

		$(document).on('click', '[data-bs-dismiss]', function (e) {  
			$('#exampleModalCenteredScrollable').hide();
		});
	});
</script>
@endsection


