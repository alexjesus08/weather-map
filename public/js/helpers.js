/*Helpers*/
function get_collapse(observations,city) {  
	var city = city.replace(' ', '-');
	var accordion_items = '';
	Object.entries(observations).forEach(entry => { 
		const [key, observation] = entry;  
		var details = '';
		Object.entries(observation).forEach(entry => { 
			const [key, observation_detail] = entry; 
			details = details + `<p><strong>${key}: </strong>${observation_detail}</p>`;
		});

		if(key!='pubDate') {
			accordion_items = accordion_items + `
			<div class="accordion-item">
				<h2 class="accordion-header" id="headingOne">
					<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-${key}-${city}" aria-controls="collapse-${key}-${city}" aria-expanded="false"> ${key} </button>
				</h2>
				<div id="collapse-${key}-${city}" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
					<div class="accordion-body"> ${details} </div>
				</div>
			</div> 
			`;
		}
	}); 


	return `
		<div style="min-width:174px;">
			<div class="accordion" id="accordionExample"> 
				${accordion_items}
			</div>
		</div>
	`;
}