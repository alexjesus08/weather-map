<p align="center"> <img src="https://viajemosappprodstorage-cdn.azureedge.net/viajemosmx/skin/frontend/viajemos/default/images/viajemos-logo.png" width="400"></p>

## Acerca de Weather Map
Esta app conecta con el API de Yahoo para traer los datos climaticos de las ciudades de Miami - Orlando - New York y mostrarlos sobre un mapa, al mismo tiempo que guarda un historico del clima de las ciudades con cada consulta en una tabla que registra date (date) - city (string) - observations (json).
Todo esto bajo un sistema MVC creaado con el Framework Laravel 8 y Bootstrap 5, usando la libreria Leaflet para mostrar estos datos climaticos sobre un mapa, y la libreria DataTable para mostrar el historico de las ciudades en una table resposive.  

## Pantalla principal 
<p align="center"> <img src="https://gitlab.com/alexjesus08/weather-map/-/raw/master/public/images/main.jpeg" width="400"></p>

## Pantalla de historico
<p align="center"> <img src="https://gitlab.com/alexjesus08/weather-map/-/raw/master/public/images/history-1.jpeg" width="400"></p>
<p align="center"> <img src="https://gitlab.com/alexjesus08/weather-map/-/raw/master/public/images/history-2.jpeg" width="400"></p>
