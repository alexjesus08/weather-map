<?php

use Illuminate\Support\Facades\Route;

/*
| Contollers
*/
Use App\Http\Controllers\WeatherController;
Use App\Http\Controllers\YahooController;

Route::get('/', [WeatherController::class, 'index']);
Route::get('/show/{city}', [WeatherController::class, 'show']); 
 
